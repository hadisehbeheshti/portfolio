
export const state = () => ({
    colors:[
      {name:'yellow',colorCod:'#ffb400'},{name:'red',colorCod:'#ff5d56'},
      {name:'blue',colorCod:'#2196F3'},
      {name:'green',colorCod:'#72b626'},{name:'orange',colorCod:'#fa5b0f'},{name:'magenta',colorCod:'#ff00ff'},
      {name:'goldenrod',colorCod:'#daa520'},{name:'yellowgreen',colorCod:'#9acd32'},

    ],
  token:''

  })
export const mutations ={

  setColor(state,color){
  const rootElement=document.querySelector(':root')
    const item=state.colors.find(cl=>cl.name==color)
      rootElement.style.setProperty('--colordefault',item.colorCod)


  },
  setToken(state,token) {
    state.token=token
  }

}
export const actions = {
    getColor({commit},color){

    commit("setColor",color)
    },
}
export  const getters = () => ({
  isAuthenticated(state) {
    return state.auth.loggedIn
  },

  loggedInUser(state) {
    return state.auth.user
  }
})
