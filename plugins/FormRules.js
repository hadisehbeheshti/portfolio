export const required = v => !!v || 'فیلد الزامی'

export const email = v => (/.+@.+\..+/.test(v) && v !== '') || 'ایمیل نامعتبر است'

export const year = v => (!isNaN(v) && v >= 1300) || 'سال نامعتبر است'

export const nationalCode = code => {
    if (!code) {
      return true
    }
    var L = code.length
    if (L < 8 || parseInt(code, 10) === 0) {
      return false
    }
    code = ('0000' + code).substr(L + 4 - 10)
    if (parseInt(code.substr(3, 6), 10) === 0) {
      return false
    }
    var c = parseInt(code.substr(9, 1), 10)
    var s = 0
    for (var i = 0; i < 9; i++) {
      s += parseInt(code.substr(i, 1), 10) * (10 - i)
    }
    s = s % 11
    return (
      (s < 2 && c === s) || (s >= 2 && c === 11 - s) || 'کد ملی معتبر نیست!'
    )
}

export const mobile = value => {
    const pattern = /(0|\+98)?([ ]|-|[()]){0,2}9[0|1|2|3|4]([ ]|-|[()]){0,2}(?:[0-9]([ ]|-|[()]){0,2}){8}/
    return pattern.test(value) || 'شماره همراه نامعتبر می باشد!'
}
export const tel = value => {
    const pattern = /\d{3}\d{8}|\d{4}-\d{7}/
    return pattern.test(value) || 'شماره تلفن نامعتبر می باشد!'
  }

export const minLength = value => {
  return value.length > 5 || 'متن پیام نباید کمتر از 5 کاراکتر باشد'
}

export const number = val => !isNaN(val) || 'حتما عدد وارد کنید'

export const percent = val => (!isNaN(val) && +val <= 100 && +val >= 0) || 'درصد وارد شده معتبر نیست'
