const contfa={
  ti1cont:' تماس ',
  ti2cont:'با من ',
  ti3cont:'آماده بحث در مورد کار طراحی محصول یا مشارکت هستم.',
  Phone:'تلفن',
  Email:'ایمیل',
  Instagram:'اینستاگرام',
  Linkedin:'لینکدین',
  subtitle:'اگر پیشنهادی دارید لطفا فرم زیر را پر کنید و من به زودی به شما پاسخ خواهم داد.',
  SocialProfiles:'شبکه های اجتماعی',
  firstname:'نام ',
  lastname:'نام خانوادگی',
  email:'ایمیل',
  subject:'موضوع',
  message:'پیام',
  send:'ارسال پیام'



}
export default contfa
