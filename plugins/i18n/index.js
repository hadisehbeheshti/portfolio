import titlefa from "./titlefa";
import titleEn from "./titleEn";
import navbarFa from "./navbarFa";
import contfa from "./contfa";
import conten from "./conten";
import resumfa from "./resumefa";
import resumen from "./resumen";
import navbarmobilefa from "./navbarmobilefa";
import detialFa from "./detialFa";
import detialEn from "./detialEn";


export default {
  locales: ['en', 'fa'],
  defaultLocale: 'fa',

  vueI18n: {
    pages: {
      about: {
        en: '/about',
        fa: '/fa/about',
      },

    },

    fallbackLocale: 'en',
    messages: {
      en: {
        ...titleEn,
        srcimg:'/_nuxt/static/image/IMG_5654.png',
        login:'LOG IN',
        logout:'LOG OUT',
        readmore:'READ MORE',
        imgabout:'/_nuxt/static/image/aboutleft.JPG',
        typeArray:['HADIS BEHESHTI','A FREELANCER','A FRONT END'],
        ...conten,
        ...resumen,
        ...detialEn





      },
      fa: {

        ...titlefa,
        srcimg:' /_nuxt/static/image/homeright.png',
        imgabout:'/_nuxt/static/image/aboutright.png',

        login: 'ورود',
        logout:'خروج',
        readmore:'بیشتر بخوانید',
        ...navbarFa,
        ...navbarmobilefa,
        typeArray:['حدیث بهشتی','فریلنسر',' فرانت اند کار'],
        ...contfa,
      ...resumfa,
        ...detialFa






      }
    }
  }

}
