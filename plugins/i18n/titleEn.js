const titleEn={
  title:'HI THERE !',
  me:'I\'M ',
  introduce:'Front End  ',
  description:'I\'m a Web programmer and Developer based in Tehran. I strives to build immersive and beautiful web applications through carefully crafted code and user-centric design.',
  btnmor:'MORE ABOUT ME',
  btnportfolio:'PORTFOLIO',
  titleab1:'ABOUT',
  titleab2:'ME',
  titleab3:'I DESIGN AND CODE BEAUTIFUL THINGS, AND I LOVE WHAT I DO.',
  titlers1:'My ',
  titlers2:'Resume',
  titlers3:'About myself and my experiences and skills',
  titlepor1:'MY ',
  titlepor2:'PORTFOLIO',
  titlecont1:'POSTS',
  titlecont2:'LATEST ',

}

export default titleEn
